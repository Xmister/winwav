#include <windows.h>
#include <stdio.h>
#include "main.h"

FILE* openWav(HWND hwnd) {
	//http://cboard.cprogramming.com/c-programming/136029-openfiledialog-window-c.html
	CHAR FileName[MAX_PATH];
	OPENFILENAME  ofn;   
	FILE* f;    
    memset(&ofn,0,sizeof(ofn));
    ofn.lStructSize     = sizeof(ofn);
    ofn.hwndOwner       = hwnd;
    ofn.hInstance       = NULL;
    ofn.lpstrFilter     = "WAV Files\0*.wav\0\0";   
    ofn.lpstrFile       = FileName;
    ofn.lpstrFile[0] 	= '\0';
    ofn.nMaxFile        = MAX_PATH;
    ofn.lpstrTitle      = "Please Select A File To Open";
    ofn.Flags           = OFN_NONETWORKBUTTON |
                          OFN_FILEMUSTEXIST;
    if (GetOpenFileName(&ofn)) {
		f=fopen(ofn.lpstrFile,"rb");
		return f;
	}
	return NULL;
}

void saveWav(HWND hwnd, wav_header* header, unsigned char* data) {
	char FileName[MAX_PATH];
	OPENFILENAME  ofn;   
	FILE* f;    
    memset(&ofn,0,sizeof(ofn));
    ofn.lStructSize     = sizeof(ofn);
    ofn.hwndOwner       = hwnd;
    ofn.hInstance       = NULL;
    ofn.lpstrFilter     = "WAV Files\0*.wav\0\0";   
    ofn.lpstrFile       = FileName;
    ofn.lpstrFile[0] 	= '\0';
    ofn.nMaxFile        = MAX_PATH;
    ofn.lpstrTitle      = "Please Select a destination";
    ofn.Flags           = OFN_NONETWORKBUTTON;
    if (GetSaveFileName(&ofn)) {
		f=fopen(ofn.lpstrFile,"wb");
		fwrite(header,sizeof(wav_header),1,f);
		fwrite(data,header->subchunk2_size,1,f);
		fclose(f);
	}
}

wav_header* getHeader(FILE* wav) {
	wav_header* header=(wav_header*) malloc(sizeof(wav_header));
	fread(header,sizeof(wav_header),1,wav);
	return header;
}

unsigned char* getData(FILE* wav,wav_header* header){
	unsigned char* data=(unsigned char*)malloc(header->subchunk2_size);
	fread(data,header->subchunk2_size,1,wav);
	fclose(wav);
	return data;
}

unsigned char* convertWav(wav_header* header,wav_header* newheader,unsigned char* data,unsigned int bps) {
	unsigned char* newdata;
	unsigned int i;
	int temp=0;
	memcpy(newheader,header,sizeof(wav_header));
	if ( header->bits_per_sample == bps) return NULL;
	if ( header->bits_per_sample == 16) {
		if (bps == 8) {
			newheader->bits_per_sample=8;
			newheader->subchunk2_size=header->subchunk2_size/2;
			newheader->block_align=header->block_align/2;
			newheader->byte_rate=header->byte_rate/2;
			newheader->chunk_size=newheader->subchunk2_size+36;
			newdata=(unsigned char*)malloc(newheader->subchunk2_size);
			for (i=0; i<newheader->subchunk2_size; i++ ) {
				temp=data[i*2]+(data[i*2+1]*256);
				temp=(temp+32768)>>8;
				newdata[i]=(unsigned char)temp;
			}
			return newdata;
		}
	}	
	if ( header->bits_per_sample == 8) {
		if (bps == 16) {
			newheader->bits_per_sample=16;
			newheader->subchunk2_size=header->subchunk2_size*2;
			newheader->block_align=header->block_align*2;
			newheader->byte_rate=header->byte_rate*2;
			newheader->chunk_size=newheader->subchunk2_size+36;
			newdata=(unsigned char*)malloc(newheader->subchunk2_size);
			for (i=0; i<header->subchunk2_size; i++ ) {	
				newdata[i*2]=(data[i])/2;
				newdata[i*2+1]=(data[i])/2;
			}
			return newdata;
		}
	}
}

unsigned char* StereoToMono(wav_header* header,wav_header* newheader,unsigned char* data)
{
	unsigned char* newdata;
	unsigned int i;
	int temp=0,temp2=0,temp3=0;
	memcpy(newheader,header,sizeof(wav_header));
	newheader->num_channels=1;
	newheader->subchunk2_size=header->subchunk2_size/2;
	newheader->block_align=header->block_align/2;
	newheader->byte_rate=header->byte_rate/2;
	newheader->chunk_size=header->subchunk2_size+36;
	newdata=(unsigned char*)malloc(newheader->subchunk2_size);
	if(newheader->bits_per_sample==8)
	{
		for (i=0; i<newheader->subchunk2_size; i++ ) {	
				temp=data[i*2]+data[i*2+1];
				temp/=2;
				newdata[i]=temp;
		}
	} else for(i=0;i<newheader->subchunk2_size; i+=2)
			{
				temp=data[i*2]|(data[i*2+1]*256);
				temp2=data[i*2+2]|(data[i*2+3]*256);
				temp3=temp+temp2;
				temp3/=2;
				temp2=temp3;
				temp2/=256;
				newdata[i+1]=(temp3)&0xFF;
				newdata[i]=(temp2)&0xFF;
			}
	return newdata;
}


/* This is where all the input to the window goes to */
LRESULT CALLBACK WndProc(HWND hwnd, UINT Message, WPARAM wParam, LPARAM lParam) {
	static FILE* wav, *temp;
	static wav_header* header;
	static unsigned char* data;
	char bits, law;
	char s[1024];
	unsigned char* newdata;
	wav_header* newheader;
	switch(Message) {
		case WM_COMMAND:
           switch( wParam )
           {
             case IDM_FILEOPEN:
             	temp=openWav(hwnd);
             	if (temp != NULL) {
             		wav=temp;
             		header=getHeader(wav);
             		data=getData(wav,header);
             	}
             	else MessageBox(hwnd,"Hiba","NULL",1);
				break;
			case IDM_FILECONV8:
				newheader=(wav_header*)malloc(sizeof(wav_header));
             	newdata=convertWav(header,newheader,data,8);
             	if (newdata != NULL) data=newdata;
             	header=newheader;
				break;
			case IDM_FILECONV16:
             	newheader=(wav_header*)malloc(sizeof(wav_header));
             	newdata=convertWav(header,newheader,data,16);
             	if (newdata != NULL) data=newdata;
             	header=newheader;
				break;
			case IDM_FILETOULAW:
				newheader=(wav_header*)malloc(sizeof(wav_header));
             	newdata=convertWav(header,newheader,data,16);
             	if (newdata != NULL) data=newdata;
             	header=newheader;
				break;
			case IDM_FILETOALAW:
				newheader=(wav_header*)malloc(sizeof(wav_header));
             	newdata=convertWav(header,newheader,data,16);
             	if (newdata != NULL) data=newdata;
             	header=newheader;
				break;
			case IDM_FILETOPCM:
				newheader=(wav_header*)malloc(sizeof(wav_header));
             	newdata=convertWav(header,newheader,data,16);
             	if (newdata != NULL) data=newdata;
             	header=newheader;
				break;
			case IDM_FILESAVE:
             	if (data != NULL ) saveWav(hwnd, header, data);
				break;
			case IDM_STEREOTOMONO:
				newheader=(wav_header*)malloc(sizeof(wav_header));
             	if (data != NULL && header->num_channels!=1) {
             		data=StereoToMono(header,newheader,data);
             		header=newheader;
        		}
				break;
			case IDM_FILEINFO:
             	if (temp != NULL) {
             		sprintf(s,"Channels: %d\nCharacteristic: %s\nSample Rate: %d\nb/s: %d\n%s",header->num_channels,
				 											(header->audio_format == 1 ? "PCM" : (header->audio_format == 6 ? "mu-Law" : (header->audio_format ==7 ? "A-Law" : "Unknown"))),
															 header->sample_rate,
															 header->bits_per_sample,
															 header->num_channels == 1 ? "Mono" : (header->num_channels == 2 ? "Stereo" : "Unknown")
															 );
             		MessageBox(hwnd,s,"INFO",1);
             	}
				break;
             case IDM_FILEEXIT:
                  SendMessage( hwnd, WM_CLOSE, 0, 0L );
                  return 0;

           }
           break;
		
		/* trap the WM_CLOSE (clicking X) message, and actually tell the window to close */
		case WM_CLOSE: {
			DestroyWindow(hwnd);
			break;
		}
		
		/* Upon destruction, tell the main thread to stop */
		case WM_DESTROY: {
			PostQuitMessage(0);
			break;
		}
		
		/* All other messages (a lot of them) are processed using default procedures */
		default:
			return DefWindowProc(hwnd, Message, wParam, lParam);
	}
	return 0;
}

/* The 'main' function of Win32 GUI programs: this is where execution starts */
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow) {
	WNDCLASSEX wc; /* A properties struct of our window */
	HWND hwnd; /* A 'HANDLE', hence the H, or a pointer to our window */
	MSG Msg; /* A temporary location for all messages */
	HMENU menu;

	/* zero out the struct and set the stuff we want to modify */
	memset(&wc,0,sizeof(wc));
	wc.cbSize		 = sizeof(WNDCLASSEX);
	wc.lpfnWndProc	 = WndProc; /* This is where we will send messages to */
	wc.hInstance	 = hInstance;
	wc.hCursor		 = LoadCursor(NULL, IDC_ARROW);
	
	/* White, COLOR_WINDOW is just a #define for a system color, try Ctrl+Clicking it */
	wc.hbrBackground = (HBRUSH)(COLOR_WINDOW+1);
	wc.lpszClassName = "WindowClass";
	wc.hIcon		 = LoadIcon(NULL, IDI_APPLICATION); /* Load a standard icon */
	wc.hIconSm		 = LoadIcon(NULL, IDI_APPLICATION);

	if(!RegisterClassEx(&wc)) {
		MessageBox(NULL, "Window Registration Failed!","Error!",MB_ICONEXCLAMATION|MB_OK);
		return 0;
	}

	hwnd = CreateWindowEx(WS_EX_CLIENTEDGE,"WindowClass","Caption",WS_VISIBLE|WS_OVERLAPPEDWINDOW,
		CW_USEDEFAULT, /* x */
		CW_USEDEFAULT, /* y */
		640, /* width */
		480, /* height */
		NULL,NULL,hInstance,NULL);

	if(hwnd == NULL) {
		MessageBox(NULL, "Window Creation Failed!","Error!",MB_ICONEXCLAMATION|MB_OK);
		return 0;
	}
	
	menu = LoadMenu(hInstance, MAKEINTRESOURCE(ID_MENU));
    SetMenu(hwnd, menu);

	/*
		This is the heart of our program where all input is processed and 
		sent to WndProc. Note that GetMessage blocks code flow until it receives something, so
		this loop will not produre unreasonably CPU usage
	*/
	while(GetMessage(&Msg, NULL, 0, 0) > 0) { /* If no error is received... */
		TranslateMessage(&Msg); /* Translate keycodes to chars if present */
		DispatchMessage(&Msg); /* Send it to WndProc */
	}
	return Msg.wParam;
}

